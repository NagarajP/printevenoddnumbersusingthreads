package com.myzee.evenodd;

import java.util.concurrent.atomic.AtomicLong;

public class PrintEvenOddTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Object lock = new Object();
		AtomicLong num = new AtomicLong(0);
		Even e = new Even(lock, num);
		Odd o = new Odd(num, lock);
		
		e.start();
		o.start();
		
	}

}

class Even extends Thread {
	Object lock;
	AtomicLong num;

	public Even(Object lock, AtomicLong num) {
		this.lock = lock;
		this.num = num;
	}

	public void run() {
		synchronized (lock) {
			while (true) {
				if (num.get() % 2 != 0) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("Even - " + num.get());
					num.incrementAndGet();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					lock.notifyAll();

				}
			}
		}
	}

}

class Odd extends Thread {
	AtomicLong num;
	Object lock;

	public Odd(AtomicLong num, Object lock) {
		this.num = num;
		this.lock = lock;
	}

	public void run() {
		synchronized (lock) {
			while (true) {
				if(num.get()%2 == 0) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println("Odd - " + num.get());
					num.incrementAndGet();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					lock.notifyAll();
				}
			}
		}
	}

}
