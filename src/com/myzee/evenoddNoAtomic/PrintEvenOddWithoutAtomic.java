package com.myzee.evenoddNoAtomic;

import java.util.concurrent.atomic.AtomicLong;

public class PrintEvenOddWithoutAtomic {

	static Integer num = new Integer(0);

	public static void main(String[] args) {
		Object lock = new Object();

		Even e = new Even(lock, true);
		Odd o = new Odd(lock, false);

		e.start();
		o.start();

	}

}

class Even extends Thread {
	Object lock;
	boolean b;

	public Even(Object lock, boolean b) {
		this.lock = lock;
		this.b = b;
	}

	public void run() {
		while (true) {
			synchronized (lock) {
				if (b == false) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			synchronized (lock) {
				System.out.println("Even - " + PrintEvenOddWithoutAtomic.num);
				PrintEvenOddWithoutAtomic.num++;
				b = false;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				lock.notifyAll();

			}

		}
	}

}

class Odd extends Thread {
	Object lock;
	boolean b;

	public Odd(Object lock, boolean b) {
		this.lock = lock;
		this.b = b;
	}

	public void run() {
		while (true) {
			synchronized (lock) {
				if (b == true) {
					try {
						lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			synchronized (lock) {
				System.out.println("Odd - " + PrintEvenOddWithoutAtomic.num);
				PrintEvenOddWithoutAtomic.num++;
				b = true;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lock.notifyAll();
			}

		}
	}

}
